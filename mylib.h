#ifndef MYLIB_H
#define MYLIB_H

#include <cmath>
#include <omp.h>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <cmath>
#include <random>
#include <chrono>

#define NTHREAD 16

template<typename T>
inline void rand(T *x,size_t n){
 unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
 std::default_random_engine generator(seed);
 std::uniform_real_distribution<T> distribution(T(0),T(1));
 #pragma omp parallel for num_threads(NTHREAD)
 for(size_t i=0;i<n;++i) x[i]=distribution(generator);
 return;
}

template<typename T>
inline void transp(T *y, T *x,size_t n1,size_t n2){
 #pragma omp parallel for num_threads(NTHREAD)
 for(size_t i1=0;i1<n1;++i1){
  for(size_t i2=0;i2<n2;++i2){
   y[i2+n2*i1]=x[i1+n1*i2];
  }
 }
 return;
}

template<typename T1,typename T2,typename T3>
inline void add(T1 *ab,const T2 *a,const T3 *b,size_t n){
 #pragma omp parallel for num_threads(NTHREAD)
 for(size_t i=0;i<n;++i) ab[i]=a[i]+b[i];
 return;
}

template<typename T1,typename T2,typename T3>
inline void subtract(T1 *ab,const T2 *a,const T3 *b,size_t n){
 #pragma omp parallel for num_threads(NTHREAD)
 for(size_t i=0;i<n;++i) ab[i]=a[i]-b[i];
 return;
}

template<typename T1,typename T2,typename T3>
inline void multiply(T1 *ab,const T2 *a,const T3 *b,size_t n){
 #pragma omp parallel for num_threads(NTHREAD)
 for(size_t i=0;i<n;++i) ab[i]=a[i]*b[i];
 return;
}

template<typename T1,typename T2,typename T3>
inline void divide(T1 *ab,const T2 *a,const T3 *b,size_t n){
 #pragma omp parallel for num_threads(NTHREAD)
 for(size_t i=0;i<n;++i) ab[i]=a[i]/b[i];
 return;
}

template<typename T1,typename T2,typename T3>
inline void divide(T1 *ab,const T2 a,const T3 *b,size_t n){
 #pragma omp parallel for num_threads(NTHREAD)
 for(size_t i=0;i<n;++i) ab[i]=a/b[i];
 return;
}

template<typename T1,typename T2,typename T3>
inline void reverse_multiply(T1 *ab,const T2 *a,const T3 *b,size_t n){
 #pragma omp parallel for num_threads(NTHREAD)
 for(size_t i=0;i<n;++i) ab[i]=a[i]*b[n-1-i];
 return;
}

template<typename T1,typename T2,typename T3>
inline void scale_add(T1 *ab,const T2 *a,T3 b,size_t n){
 #pragma omp parallel for num_threads(NTHREAD)
 for(size_t i=0;i<n;++i) ab[i]+=a[i]*b;
 return;
}

template<typename T1,typename T2,typename T3>
inline void scale_add(T1 *ab,T2 b,const T3 *a,size_t n){
 #pragma omp parallel for num_threads(NTHREAD)
 for(size_t i=0;i<n;++i) ab[i]+=a[i]*b;
 return;
}

template<typename T1,typename T2>
inline void scale(T1 *a,T2 b,size_t n){
 #pragma omp parallel for num_threads(NTHREAD)
 for(size_t i=0;i<n;++i) a[i]*=b;
 return;
}

template<typename T1,typename T2,typename T3>
inline void scale(T1 *ab,const T2 *a,T3 b,size_t n){
 #pragma omp parallel for num_threads(NTHREAD)
 for(size_t i=0;i<n;++i) ab[i]=a[i]*b;
 return;
}

template<typename T1,typename T2,typename T3>
inline void scale(T1 *ab,T2 a,const T3 *b,size_t n){
 #pragma omp parallel for num_threads(NTHREAD)
 for(size_t i=0;i<n;++i) ab[i]=a*b[i];
 return;
}

template<typename T1,typename T2,typename T3,typename T4,typename T5>
inline void lin_comb(T1 *z,T2 a,const T3 *x,T4 b,const T5 *y,size_t n){
 #pragma omp parallel for num_threads(NTHREAD) 
 for(size_t i=0;i<n;++i) z[i]=a*x[i]+b*y[i];
 return;
}

template<typename T1,typename T2,typename T3>
inline void shift(T1 *ab,const T2 *a,T3 b,size_t n){
 #pragma omp parallel for num_threads(NTHREAD)
 for(size_t i=0;i<n;++i) ab[i]=a[i]+b;
 return;
}

template<typename T1,typename T2,typename T3>
inline void shift(T1 *ab,T2 a,const T3 *b,size_t n){
 #pragma omp parallel for num_threads(NTHREAD)
 for(size_t i=0;i<n;++i) ab[i]=a+b[i];
 return;
}

template<typename T1,typename T2,typename T3>
inline void pow(T1 *ab,const T2 *a,T3 b,size_t n){
 #pragma omp parallel for num_threads(NTHREAD) 
 for(size_t i=0;i<n;++i) ab[i]=pow(a[i],b);
 return;
}

template<typename T1,typename T2,typename T3>
inline void pow(T1 *ab,T2 a,const T3 *b,size_t n){
 #pragma omp parallel for num_threads(NTHREAD) 
 for(size_t i=0;i<n;++i) ab[i]=pow(a,b[i]);
 return;
}

template<typename T1,typename T2>
inline void sqrt(T1 *a,const T2 *b,size_t n){
 #pragma omp parallel for num_threads(NTHREAD) 
 for(size_t i=0;i<n;++i) a[i]=sqrt(b[i]);
 return;
}

template<typename T1,typename T2>
inline void cbrt(T1 *a,const T2 *b,size_t n){
 #pragma omp parallel for num_threads(NTHREAD) 
 for(size_t i=0;i<n;++i) a[i]=cbrt(b[i]);
 return;
}

template<typename T1,typename T2>
inline void exp(T1 *a,const T2 *b,size_t n){
 #pragma omp parallel for num_threads(NTHREAD) 
 for(size_t i=0;i<n;++i) a[i]=exp(b[i]);
 return;
}

template<typename T1,typename T2>
inline void reciprocal(T1 *a,const T2 *b,size_t n){
 #pragma omp parallel for num_threads(NTHREAD) 
 for(size_t i=0;i<n;++i) a[i]=1./b[i];
 return;
}

template<typename T1,typename T2>
inline void log(T1 *a,const T2 *b,size_t n){
 #pragma omp parallel for num_threads(NTHREAD)
 for(size_t i=0;i<n;++i) a[i]=log(b[i]);
 return;
}

template<typename T1,typename T2>
inline void equate(T1 *a,const T2 *b,size_t n){
 #pragma omp parallel for num_threads(NTHREAD)
 for(size_t i=0;i<n;++i) a[i]=b[i];
 return;
}

template<typename T1,typename T2>
inline void mynegate(T1 *a,const T2 *b,size_t n){
 #pragma omp parallel for num_threads(NTHREAD) 
 for(size_t i=0;i<n;++i) a[i]=-b[i];
 return;
}

template<typename T1,typename T2>
inline void set(T1 *a,T2 b,size_t n){
 #pragma omp parallel for num_threads(NTHREAD)
 for(size_t i=0;i<n;++i) a[i]=b;
 return;
}

template<typename T>
inline T kahan_sum(const T *a,size_t n){
    T s=0, c=0;
    for(size_t i=0; i<n; i++){
        T y=a[i]-c;
        T t=s+y;
        T z=t-s;
        c=z-y;
        s=t;
    }
    return s;
}

template<typename T>
inline T norm(const T *a,size_t n){
    T s=0, c=0;
    for(size_t i=0; i<n; i++){
        T y=a[i]*a[i]-c;
        T t=s+y;
        T z=t-s;
        c=z-y;
        s=t;
    }
    return std::sqrt(double(s));
}

template<typename T1,typename T2>
inline double kahan_dot_product(const T1 *a,const T2 *b,size_t n){
    double s=0, c=0;
    for(size_t i=0; i<n; i++){
        double y=a[i]*b[i]-c;
        double t=s+y;
        double z=t-s;
        c=z-y;
        s=t;
    }
    return s;
}

template<typename T1,typename T2>
inline double relative_l2_error(const T1 *a,const T2 *b,size_t n){
    double s=0, c=0;
    for(size_t i=0; i<n; i++){
        double ab=a[i]-b[i];
        double y=ab*ab-c;
        double t=s+y;
        double z=t-s;
        c=z-y;
        s=t;
    }
    double norma=norm(a,n), normb=norm(b,n), normab=std::sqrt(s);
    return 2*normab/(norma+normb);
}

template<typename T>
inline T sum(const T *a,size_t n){
 T s=0;
 #pragma omp parallel for reduction(+:s) num_threads(NTHREAD)
 for(size_t i=0;i<n;++i) s+=a[i];
 return s;
}

template<typename T1,typename T2>
inline double dot_product(const T1 *a,const T2 *b,size_t n){
 double s=0.;
 #pragma omp parallel for num_threads(NTHREAD) reduction(+:s) 
 for(size_t i=0;i<n;++i) s+=a[i]*b[i];
 return s;
}

template<typename T>
inline T min(const T *a,size_t n){
 T m=a[0];
 for(size_t i=1;i<n;++i){
     if(!std::isfinite(a[i])){
       fprintf(stderr,"not a finite number at index %zd\n",i);
       break;
     }
     else if(a[i]<m) m=a[i];
 }
 return m;
}

template<typename T>
inline T max(const T *a,size_t n){
 T m=a[0];
 for(size_t i=1;i<n;++i){
     if(!std::isfinite(a[i])){
       fprintf(stderr,"not a finite number at index %zd\n",i);
       break;
     }
     else if(a[i]>m) m=a[i];
 }
 return m;
}

template<typename T>
inline T max_abs(const T *a,size_t n){
 T m=fabs(a[0]);
 for(size_t i=1;i<n;++i){
     if(!std::isfinite(a[i])){
       fprintf(stderr,"not a finite number at index %zd\n",i);
       break;
     }
     else if(fabs(a[i])>m) m=fabs(a[i]);
 }
 return m;
}

template <typename T> 
inline size_t sign(T val){
 return (T(0) < val) - (val < T(0));
}

template <typename T> void print(const std::string &s,const T *x,size_t n){
    std::cout<<s<<std::endl;
    for(size_t i=0;i<n;i++) std::cout<<"i="<<i<<" value="<<x[i]<<std::endl;
    std::cout<<std::endl;
    return;
}

template<typename T>
inline void print(const T *x,size_t n){
    for(size_t i=0; i<n; i++) std::cout<<x[i]<<" ";
    std::cout<<std::endl;
}

template<typename T>
inline void print(const T *x,size_t nrow,size_t ncol){
    for(size_t i=0; i<nrow; i++) print(x+i*ncol,ncol);
    std::cout<<std::endl;
}

template<typename T>
inline void print(size_t n, const T *x,size_t ld){
    for(size_t i=0, k=0; i<n; i++, k+=ld) printf("%.5e ", x[k]);
    printf("\n");
}

template<typename T>
inline void print(size_t nrow,size_t ncol,const T *x){
    for(size_t i=0; i<nrow; i++) print(ncol, x+i, nrow); 
    printf("\n");
}

template<typename T>
inline T relative_infty_norm_error(const T *x,const T *y,size_t n){
    T e(0), normx(0), normy(0);
    for(size_t i=0; i<n; i++){
        T a=abs(x[i]); if(a>normx) normx=a;
        a=abs(y[i]); if(a>normy) normy=a;
        a=abs(x[i]-y[i]); if(a>e) e=a;
    }
}

#endif
