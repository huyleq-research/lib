#include <cstdlib>
#include "myio.h"

using namespace std;

int main(int argc,char **argv){
 myio_init(argc,argv);
 int n1,n2;
 float d1,d2,o1,o2;
 from_header("infile","n1",n1,"o1",o1,"d1",d1);
 from_header("infile","n2",n2,"o2",o2,"d2",d2);
 size_t n=n1*n2;
 float *x=new float[n](); read("infile",x,n);
 float *y=new float[n](); bswap(y,x,n);
 write("outfile",y,n);
 to_header("outfile","n1",n1,"o1",o1,"d1",d1);
 to_header("outfile","n2",n2,"o2",o2,"d2",d2);
 delete []x;delete []y;
 myio_close();
 return 0;
}
