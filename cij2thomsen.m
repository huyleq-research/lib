function [vp,vs,eps,del,gam]=cij2thomsen(c11,c33,c44,c12,c13,rho)
vp=sqrt(c33/rho);
vs=sqrt(c44/rho);
eps=(c11-c33)/2/c33;
c66=(c11-c12)/2;
gam=(c66-c44)/2/c44;
del=((c13+c44)^2-(c33-c44)^2)/2/c33/(c33-c44);
C=[c11 c12 c13 0 0 0;
   c12 c11 c13 0 0 0;
   c13 c13 c33 0 0 0;
   0 0 0 c44 0 0;
   0 0 0 0 c44 0;
   0 0 0 0 0 c66];
e=eig(C);
if min(e)==0
    fprintf('zero eigenvalue\n');
end
if min(e)<0
    fprintf('negative eigenvalue\n');
end
end