#include <iostream>
#include <cstdio>

int main(){
 int deviceCount;
 cudaGetDeviceCount(&deviceCount);
 std::cout<<"Total # of GPUs "<<deviceCount<<std::endl;
 for(int device=0;device<deviceCount;++device){
  cudaDeviceProp deviceProp;
  cudaGetDeviceProperties(&deviceProp,device);
  printf("Device %d has compute capacity %d.%d.\n",device,deviceProp.major,deviceProp.minor);
  int asyncCap=deviceProp.asyncEngineCount;
  printf("asyncEngineCount=%d.\n",asyncCap);
  if(asyncCap>0) printf("Device %d can do concurrent data transfer and kernel execution.\n",device);
  if(asyncCap==2) printf("Device %d can do concurrent data transfer.\n",device);
  int concCap=deviceProp.concurrentKernels;
  if(concCap>0) printf("Device %d can do concurrent kernel execution.\n",device);
 }
 return 0;
}
