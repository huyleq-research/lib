#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include "myio.h"
#include "mylib.h"

using namespace std;

int main(int argc,char **argv){
    myio_init(argc,argv);

    int n=10;
     
    double *x=new double[n];
    for(int i=0;i<n;i++) x[i]=i;
    
    write("x",x,n,"native_double");
    to_header("x","n1",n,"o1",0.,"d1",1.);

    double *y=new double[n];
    read("x",y,n);
    for(int i=0;i<n;i++) fprintf(stderr,"%f\n",y[i]);

    delete []x;delete []y;
     
    myio_close();
     
    return 0;
}
