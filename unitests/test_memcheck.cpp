#include <iostream>
#include "memcheck.h"

using namespace std;

int main(){
 int n=10;
 myPtr<float> *x=new myPtr<float>[2];
 x[0]=myPtr<float>(n*sizeof(float));
 x[1]=myPtr<float>(n*sizeof(float));
 cout<<myPtr<float>::total<<endl;
 for(int i=0;i<n;++i){
  *(x[0].get_ptr()+i)=i;
  cout<<*(x[0].get_ptr()+i)<<endl;
 }
 for(int i=0;i<n;++i){
  *(x[1].get_ptr()+i)=*(x[0].get_ptr()+i);
  cout<<*(x[1].get_ptr()+i)<<endl;
 }
 x[0].myFree();
 x[1].myFree();
 cout<<myPtr<float>::total<<endl;
 delete []x;
 return 0;
}
