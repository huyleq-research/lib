#include <iostream>
#include <omp.h>
#include <cstdlib>
#include <time.h>
#include <stdio.h>
#include "mylib.h"

using namespace std;

int main(){
 int n=10000;
 float *a=new float[n]();
 srand(time(NULL));
 float b=(float)rand()/RAND_MAX;
 set(a,b,n);
 for(int i=0;i<n;++i) if(a[i]!=b) { cout<<"something is wrong with set"<<endl; break; }
 delete []a;
 return 0;
}
