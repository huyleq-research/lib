#include <iostream>
#include <omp.h>
#include <cstdlib>
#include <time.h>
#include <stdio.h>
#include "mylib.h"

using namespace std;

int main(){
 int n=10000;
 float *a=new float[n]();
 float s=0.;
 srand(time(NULL));
 for(int i=0;i<n;++i){
  a[i]=(float)rand()/RAND_MAX;
  s+=a[i];
 }
 float ss=sum(a,n);
 cout<<"Manually: "<<s<<endl;
 cout<<"By sum function: "<<ss<<endl;
 delete []a;
 return 0;
}
