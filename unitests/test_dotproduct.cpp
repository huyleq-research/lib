#include <iostream>
#include <omp.h>
#include <cstdlib>
#include <time.h>
#include <stdio.h>
#include "mylib.h"

using namespace std;

int main(){
 int n=1600*400;
 float *a=new float[n]();
 float *b=new float[n]();
 double s=0.;
 srand(time(NULL));
 for(int i=0;i<n;++i){
  a[i]=((float)rand()/RAND_MAX);
  b[i]=((float)rand()/RAND_MAX);
  s+=a[i]*b[i];
 }
 double ss=dot_product(a,b,n);
 fprintf(stderr,"mannually %.10f. mylib %.10f\n",s,ss);
 delete []a,b;
 return 0;
}
