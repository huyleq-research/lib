#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include "myio.h"
#include "mylib.h"

using namespace std;

int main(int argc,char **argv){
 myio_init(argc,argv);
 
 int nx,nz;
 float dx,dz,ox,oz;
 
 cout<<"getting parameters"<<endl;
// if(get_param("nx",nx)) cout<<"nx="<<nx<<endl;
// if(get_param("nz",nz)) cout<<"nz="<<nz<<endl;
// if(get_param("dx",dx)) cout<<"dx="<<dx<<endl;
 if(get_param("dz",dz)) cout<<"dz="<<dz<<endl;
 if(get_param("ox",ox)) cout<<"ox="<<ox<<endl;
 if(get_param("oz",oz)) cout<<"oz="<<oz<<endl;
 get_param("nx",nx,"nz",nz,"dx",dx);
 cout<<"nx="<<nx<<" "<<"nz="<<nz<<" "<<"dx="<<dx<<endl;
 cout<<"done getting parameters"<<endl;

 float *x=new float[nx*nz]();
 for(int iz=0;iz<nz/2;++iz){
  for(int ix=0;ix<nx;++ix){
   x[ix+iz*nx]=2.f;
  }
 }
 for(int iz=nz/2;iz<nz;++iz){
  for(int ix=0;ix<nx;++ix){
   x[ix+iz*nx]=4.f;
  }
 }
 
 cout<<"writing a 2 layer-model, top=2,bottom=4"<<endl;
 write("x",x,nx*nz/2,"xdr_float",ios_base::app);
 write("x",x+nx*nz/2,nx*nz/2,"xdr_float",ios_base::app);
// scale(x,x,2.,nx*nz);
// write("x",x,nx*nz,ios_base::app);
 cout<<"done writing"<<endl;
 
 cout<<"writing header"<<endl;
 to_header("x","n1",nx,"o1",ox,"d1",dx);
 to_header("x","n2",nz,"o2",oz,"d2",dz);
// to_header("x","n3",2,"o3",0,"d3",1);
 from_header("x","n2",nz,"o2",oz,"d2",dz);
 cout<<"nz="<<nz<<" "<<"oz="<<oz<<" "<<"dz="<<dz<<endl;
 
 cout<<"done writing header"<<endl;

 int n1,n2;
 float o1,o2,d1,d2;
 
 cout<<"reading parameters from header"<<endl;
 if(from_header("y","n1",n1)) cout<<"n1="<<n1<<endl;
 if(from_header("y","n2",n2)) cout<<"n2="<<n2<<endl;
 if(from_header("y","d1",d1)) cout<<"d1="<<d1<<endl;
 if(from_header("y","d2",d2)) cout<<"d2="<<d2<<endl;
 if(from_header("y","o1",o1)) cout<<"o1="<<o1<<endl;
 if(from_header("y","o2",o2)) cout<<"o2="<<o2<<endl;
 cout<<"done reading parameters from header"<<endl;

 float *y=new float[n1*n2]();
 cout<<"reading model"<<endl;
 read("y",y,n1*n2/2);
 read("y",y+n1*n2/2,n1*n2/2,n1*n2/2);
 cout<<"done reading model"<<endl;
 for(int iz=0;iz<nz;++iz){
  for(int ix=0;ix<nx;++ix){
   if(y[ix+iz*nx]!=x[ix+iz*nx]){
    cout<<"something is wrong"<<endl;
	break;
   }
  }
 }
 cout<<"reading correctly"<<endl;
 delete []x; delete []y;

 cout<<"reading parameters from header z"<<endl;
 from_header("z","n1",n1,"o1",o1,"d1",d1);
 from_header("z","n2",n2,"o2",o2,"d2",d2);
 cout<<n1<<" "<<o1<<" "<<d1<<endl;
 cout<<n2<<" "<<o2<<" "<<d2<<endl;
 myio_close();
 
 return 0;
}
