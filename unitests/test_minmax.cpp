#include <iostream>
#include <omp.h>
#include "mylib.h"
#include <cstdlib>
#include <time.h>
#include <stdio.h>

using namespace std;

int main(){
 int n=1000000;
 float *a=new float[n]();
 srand(time(NULL));
 for(int i=0;i<n;++i){
  a[i]=(float)rand()/RAND_MAX;
 }
 float m=a[0],mm=a[0];
 for(int i=1;i<n;++i){
  if(a[i]<m) m=a[i];
  if(a[i]>mm) mm=a[i];
 }
 float m1=min(a,n),mm1=max(a,n);
 if(m1!=m||mm1!=mm) cout<<"something is wrong "<<endl;
 delete []a;
 return 0;
}
