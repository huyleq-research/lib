#include <iostream>
#include <omp.h>
#include <cstdlib>
#include <time.h>
#include <stdio.h>
#include <cmath>
#include "mylib.h"

using namespace std;

int main(){
 int n=10000;
 float *a=new float[n]();
 float *c1=new float[n]();
 float *c2=new float[n]();
 srand(time(NULL));
 float b=(float)rand()/RAND_MAX;
 for(int i=0;i<n;++i){
  a[i]=(float)rand()/RAND_MAX;
  c1[i]=pow(b,a[i]);
 }
 pow(c2,b,a,n);
 pow(a,b,a,n);
 for(int i=0;i<n;++i) if(c2[i]-c1[i]>1e-6||a[i]-c1[i]>1e-6) { cout<<"something is wrong with pow"<<endl; break; }
 delete []a,c1,c2;
 return 0;
}
