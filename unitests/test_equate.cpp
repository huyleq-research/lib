#include <iostream>
#include <omp.h>
#include <cstdlib>
#include <time.h>
#include <stdio.h>
#include "mylib.h"

using namespace std;

int main(){
 int n=10000;
 float *a=new float[n]();
 float *b=new float[n]();
 srand(time(NULL));
 for(int i=0;i<n;++i){
  a[i]=(float)rand()/RAND_MAX;
 }
 equate(b,a,n);
 for(int i=0;i<n;++i) if(b[i]!=a[i]) { cout<<"something is wrong with equate"<<endl; break; }
 delete []a,b;
 return 0;
}
