#include <vector>
#include "myio.h"

using namespace std;

int main(int argc,char **argv){
 myio_init(argc,argv);

 vector<float> v;
 int n=100;
 for(int i=0;i<n;++i) v.push_back(0.01*(rand()%100));
 write("v",&v[0],v.size());
 to_header("v","n1",n,"d1",1.,"o1",0.);

 myio_close();
 return 0;
}
