#include <iostream>
#include <omp.h>
#include <cstdlib>
#include <time.h>
#include <stdio.h>
#include <chrono>

#include "mylib.h"

using namespace std;

int main(){
 int n=1e9;
 
 float *a=new float[n]();
 float *b=new float[n]();
 float *c1=new float[n]();
 float *c2=new float[n]();
 float *c3=new float[n]();
 
 srand(time(NULL));
 for(int i=0;i<n;++i){
  a[i]=(float)rand()/RAND_MAX;
  b[i]=(float)rand()/RAND_MAX;
 }
 
 chrono::high_resolution_clock::time_point start=chrono::high_resolution_clock::now();
 for(int i=0;i<n;++i) c1[i]=a[i]+b[i];
 chrono::high_resolution_clock::time_point end=chrono::high_resolution_clock::now();
 chrono::duration<double> time=chrono::duration_cast<chrono::duration<double> >(end-start);
 cout<<"for loop total time "<<time.count()/60.<<" minutes"<<endl;

 start=chrono::high_resolution_clock::now();
 add(c2,a,b,n);
 end=chrono::high_resolution_clock::now();
 time=chrono::duration_cast<chrono::duration<double> >(end-start);
 cout<<"add function total time "<<time.count()/60.<<" minutes"<<endl;

 start=chrono::high_resolution_clock::now();
 add1(c3,a,b,n);
 end=chrono::high_resolution_clock::now();
 time=chrono::duration_cast<chrono::duration<double> >(end-start);
 cout<<"add function total time "<<time.count()/60.<<" minutes"<<endl;

 for(int i=0;i<n;++i){
    if(c2[i]!=c1[i]) cout<<"something is wrong with add"<<endl;
    break;
 }

 for(int i=0;i<n;++i){
    if(c3[i]!=c1[i]) cout<<"something is wrong with add1"<<endl;
    break;
 }

 delete []a; delete []b;delete []c1;delete []c2;delete []c3;
 return 0;
}
