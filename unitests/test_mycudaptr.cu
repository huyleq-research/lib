#include <iostream>
#include "myCudaPtr.h"

using namespace std;

int main(){
 int n=2e6;
 cudaSetDevice(0);
 myCudaPtr<float> *x=new myCudaPtr<float>[2];
 x[0]=myCudaPtr<float>(n*sizeof(float));
 x[1]=myCudaPtr<float>(n*sizeof(float));
 cout<<myCudaPtr<float>::total<<endl;
 x[0].myCudaFree();
// x[1].myCudaFree();
 cout<<myCudaPtr<float>::total<<endl;
 cudaDeviceReset();
 delete []x;
 return 0;
}
