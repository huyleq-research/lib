#include <iostream>
#include <vector>
#include <string>

#include "myio.h"

using namespace std;

int main(int argc,char **argv){
 myio_init(argc,argv);

 vector<int> gpu;
 get_array("gpu",gpu);
 for(auto i:gpu) cout<<i<<endl;

 vector<int> v1;
 get_array("n",v1);
 for(auto i:v1) cout<<i<<endl;

 vector<float> v2;
 get_array("n",v2);
 for(auto i:v2) cout<<i<<endl;

 vector<string> v3;
 get_sarray("server",v3);
 for(auto i:v3) cout<<i<<endl;

 vector<string> v4;
 get_sarray("script",v4,";");
 for(auto i:v4) cout<<i<<endl;

 myio_close();
 return 0;
}
