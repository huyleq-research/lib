#include <iostream>
#include <omp.h>
#include <cstdlib>
#include <time.h>
#include <stdio.h>
#include <cmath>
#include "mylib.h"

using namespace std;

int main(){
 int n=10000;
 float *a=new float[n]();
 float *b=new float[n]();
 float *c=new float[n]();
 srand(time(NULL));
 for(int i=0;i<n;++i){
  a[i]=(float)rand()/RAND_MAX;
  c[i]=sqrt(a[i]);
 }
 sqrt(b,a,n);
 sqrt(a,a,n);
 for(int i=0;i<n;++i) if(b[i]-c[i]>1e-6||a[i]-c[i]>1e-6) { cout<<"something is wrong with sqrt"<<endl; break; }
 delete []a,b,c;
 return 0;
}
