#include <vector>
#include <tbb/tbb.h>
#include <tbb/blocked_range.h>
#include "assign.h"
#include "parallel_memcpy.h"

using namespace std;

#define nthreads 16 

float *tbb_memcpy(float *dest,float *source,size_t n){
 tbb::parallel_for(tbb::blocked_range<int>(0,n),[&](const tbb::blocked_range<int>&r){
  assign(dest+r.begin(),source+r.begin(),r.end()-r.begin());});
 return dest;
}

float *omp_memcpy(float *dest,float *source,size_t n){
 size_t m=n/nthreads;
 int r=n%nthreads;
 #pragma omp parallel for num_threads(nthreads)
 for(int i=0;i<nthreads;++i){
  if(i<r) assign(dest+i*(m+1),source+i*(m+1),m+1);
  else assign(dest+i*m+r,source+i*m+r,m);
 }
 return dest;
}


