#include <iostream>
#include <cstdio>
#include <chrono>

using namespace std;

int main(int argc,char **argv){
 int n=4*1024*1024;
 size_t nbytes=n*sizeof(float);

 float *d_x,*d_y,*x,*y;
 x=new float[n]();
 y=new float[n]();
 cudaMalloc(&d_x,nbytes);
 cudaMalloc(&d_y,nbytes);

 cudaMemcpy(d_x,x,nbytes,cudaMemcpyHostToDevice);
 cudaMemcpy(d_y,d_x,nbytes,cudaMemcpyDeviceToDevice);
 cudaMemcpy(y,d_y,nbytes,cudaMemcpyDeviceToHost);

 chrono::high_resolution_clock::time_point start=chrono::high_resolution_clock::now();
 
 memcpy(x,y,nbytes);

 chrono::high_resolution_clock::time_point end=chrono::high_resolution_clock::now();
 chrono::duration<double> time=chrono::duration_cast<chrono::duration<double> >(end-start);
 cout<<"bandwidth "<<nbytes/1024./1024./1024./time.count()<<endl;

 start=chrono::high_resolution_clock::now();
 
 float *z;
 cudaHostAlloc(&z,nbytes,cudaHostAllocDefault);
 cudaFreeHost(z);

 end=chrono::high_resolution_clock::now();
 time=chrono::duration_cast<chrono::duration<double> >(end-start);
 cout<<"time to allocate and deallocate "<<nbytes/1024./1024./1024.<<" GB pinned memory is "<<time.count()<<endl;

 start=chrono::high_resolution_clock::now();
 
 float *zz=new float[n]();
 delete []zz;

 end=chrono::high_resolution_clock::now();
 time=chrono::duration_cast<chrono::duration<double> >(end-start);
 cout<<"time to allocate and deallocate "<<nbytes/1024./1024./1024.<<" GB pageable memory is "<<time.count()<<endl;
  
 delete []x; delete []y;
 cudaFree(d_x);
 cudaFree(d_y);
 
 return 0;
}
