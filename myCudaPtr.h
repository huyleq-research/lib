#ifndef MYCUDAPTR_H
#define MYCUDAPTR_H

template<typename T> 
class myCudaPtr{
public:
 myCudaPtr(){}

 myCudaPtr(size_t s):size(s){
  cudaMalloc((void**)&ptr,s);
  total+=(float)s*1e-6;
 }
 
 myCudaPtr(const myCudaPtr &x){
  ptr=x.ptr;
  size=x.size;
 }

 myCudaPtr & operator=(const myCudaPtr &x){
  ptr=x.ptr;
  size=x.size;
  return *this;
 }

 ~myCudaPtr(){}
 
 T *get_ptr(){return ptr;}
 
 size_t get_size(){return size;}

 void myCudaFree(){
  cudaFree((void*)ptr);
  total-=(float)size*1e-6;
 }

 static float total;
private:
 T *ptr;
 size_t size;
};

template<typename T> float myCudaPtr<T>::total=0.;

#endif
