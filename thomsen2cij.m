function [c11,c33,c44,c12,c13]=thomsen2cij(vp,vs,eps,del,gam,rho)
c33=rho*vp^2;
c44=rho*vs^2;
c11=c33*(1+2*eps);
c66=c44*(1+2*gam);
c12=c11-2*c66;
c13=sqrt((c33-c44)*((1+2*del)*c33-c44))-c44;
C=[c11 c12 c13 0 0 0;
   c12 c11 c13 0 0 0;
   c13 c13 c33 0 0 0;
   0 0 0 c44 0 0;
   0 0 0 0 c44 0;
   0 0 0 0 0 c66];
e=eig(C)
if min(e)==0
    fprintf('zero eigenvalue\n');
end
if min(e)<0
    fprintf('negative eigenvalue\n');
end
end