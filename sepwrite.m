function sepwrite(name,buf,n,o,d)
datapath='/net/torch/scr1/huyle/';
biname=strcat(datapath,name,'@');
fileID=fopen(biname,'w');
fwrite(fileID,buf,'real*4','b');
fclose(fileID);
fileID=fopen(name,'w');
for i=1:size(n,1)
    fprintf(fileID,'n%d=%d\n',i,n(i));
    fprintf(fileID,'o%d=%f\n',i,o(i));
    fprintf(fileID,'d%d=%f\n',i,d(i));
end
temp=strcat('in=',biname,'\n');
fprintf(fileID,temp);
fprintf(fileID,'data_format=xdr_float\n');
fclose(fileID);
end