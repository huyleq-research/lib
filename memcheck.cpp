#include <iostream>
#include <string>
#include <sys/types.h>
#include <unistd.h>

#include "myio.h"
#include "memcheck.h"

using namespace std;

void memcheck(){
 int id=getpid();
 string procfile="/proc/"+to_string((long long)id)+"/status";
 ifstream in;
 if(!open_file(in,procfile)){
  cout<<"cannot open file "<<procfile<<endl;
 }
 else{
  string line;
  while(getline(in,line)){
   cout<<line<<endl;
  }
 }
 close_file(in);
 return;
}
