#ifndef MEMCHECK_H
#define MEMCHECK_H

#include <cstdlib>

template<typename T> 
class myPtr{
public:
 myPtr(){}

 myPtr(size_t s):size(s){
  ptr=(T *)malloc(s);
  total+=(float)s*1e-6;
 }
 
 myPtr(const myPtr &x){
  ptr=x.ptr;
  size=x.size;
 }

 myPtr & operator=(const myPtr &x){
  ptr=x.ptr;
  size=x.size;
  return *this;
 }

 ~myPtr(){}
 
 T *get_ptr(){return ptr;}
 
 size_t get_size(){return size;}

 void myFree(){
  free((void*)ptr);
  total-=(float)size*1e-6;
 }

 static float total;
private:
 T *ptr;
 size_t size;
};

template<typename T> float myPtr<T>::total=0.;

void memcheck();

#endif
