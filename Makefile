CC=g++ -std=c++11
NVCC=/usr/local/cuda-9.0/bin/nvcc --std=c++11 -Xcompiler -fopenmp -lgomp -O3 -arch=sm_70

default: myio.o memcheck.o

%.o: %.cpp
	${CC} -c $*.cpp -o $@
		
byteswap.x: byteswap.o myio.o
	$(CC) $^ -o $@

%.x: %.cu
	$(NVCC) $< -o $@

clean:
	rm -rf *.o *.x *.H 
