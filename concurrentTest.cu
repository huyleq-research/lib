#include <iostream>
#include <chrono>
#include <cuda_profiler_api.h>
#include "myio.h"

#define k 200
#define m 5

using namespace std;

__global__ void foo(float *x){
 int i=threadIdx.x+blockIdx.x*blockDim.x;
 for(int j=0;j<k;++j) x[i]+=j+i;
 return;
}

int main(int argc,char **argv){
 myio_init(argc,argv);
 
 int n1=256,n2=1024;
 unsigned int n=n1*n2;
 const unsigned int nbytes=n*sizeof(float);

 int ratio=2,mm=(m-1)/ratio+1;
 float *c=new float[n*mm]();
 
 float *d_x;
 cudaMalloc(&d_x,nbytes);
 cudaMemset(d_x,0,nbytes);
 
 float *d_a;
 cudaMalloc(&d_a,nbytes);

 float *d_b;
 cudaMalloc(&d_b,nbytes);

 float *a;
 cudaHostAlloc(&a,nbytes,cudaHostAllocDefault);

 float *b;
 cudaHostAlloc(&b,nbytes,cudaHostAllocDefault);

 cudaStream_t compStream,transfStream;
 cudaStreamCreate(&compStream);
 cudaStreamCreate(&transfStream);
 
 chrono::high_resolution_clock::time_point start=chrono::high_resolution_clock::now();
 
 foo<<<n/32,32>>>(d_x);
 
 cudaMemcpy(d_a,d_x,nbytes,cudaMemcpyDeviceToDevice);
 
 for(int i=1;i<m;++i){
  foo<<<n/32,32,0,compStream>>>(d_x);
  
  if(i%ratio==1){
   float *pt=d_a;d_a=d_b;d_b=pt;
   cudaMemcpyAsync(b,d_b,nbytes,cudaMemcpyDeviceToHost,transfStream);
  }

  if(i%ratio==0){
   cudaMemcpyAsync(d_a,d_x,nbytes,cudaMemcpyDeviceToDevice,compStream);
   cudaStreamSynchronize(transfStream);
   float *pt=a;a=b;b=pt;
   memcpy(c+(i/ratio-1)*n,a,nbytes); 
   cudaStreamSynchronize(compStream);
  }
 }

 cudaMemcpy(c+(m-1)/ratio*n,d_a,nbytes,cudaMemcpyDeviceToHost);
 
// foo<<<n/32,32>>>(d_x);
//  
// cudaMemcpy(d_a,d_x,nbytes,cudaMemcpyDeviceToDevice);
//  
// for(int i=1;i<m;++i){
//  float *pt=d_a;d_a=d_b,d_b=pt;
//
//  foo<<<n/32,32,0,compStream>>>(d_x);
//  
//  cudaMemcpyAsync(d_a,d_x,nbytes,cudaMemcpyDeviceToDevice,compStream);
//  
//  cudaMemcpyAsync(b,d_b,nbytes,cudaMemcpyDeviceToHost,transfStream);
//  
//  if(i>1) memcpy(c+(i-2)*n,a,nbytes);
//  
//  cudaStreamSynchronize(transfStream);
//  cudaStreamSynchronize(compStream);
//  
//  pt=a;a=b;b=pt;
// }
//  cudaMemcpyAsync(b,d_a,nbytes,cudaMemcpyDeviceToHost,transfStream);
//  
//  memcpy(c+(m-2)*n,a,nbytes);
//  
//  cudaStreamSynchronize(transfStream);
//  memcpy(c+(m-1)*n,b,nbytes);

 write("out",c,n*mm);
 to_header("out","n1",n1,"o1",0.,"d1",1.);
 to_header("out","n2",n2,"o2",0.,"d2",1.);
 to_header("out","n3",mm,"o3",0.,"d3",1.);
 
 chrono::high_resolution_clock::time_point end=chrono::high_resolution_clock::now();
 chrono::duration<double> time=chrono::duration_cast<chrono::duration<double> >(end-start);
 cout<<"total time "<<time.count()<<" seconds"<<endl;
 
 cudaStreamDestroy(compStream);
 cudaStreamDestroy(transfStream);

 delete []c;
 cudaFree(d_x);
 cudaFree(d_a);
 cudaFree(d_b);
 cudaFreeHost(a);
 cudaFreeHost(b);
 
 cudaProfilerStop();

 myio_close();

 return 0;
}
